const router = require("express").Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const auth = require("../middleware/auth");
const config = require("config");
var fs = require("fs");
const axios = require("axios");
var path = require("path");
const Products = require("../models/products.model");
const Retailers = require("../models/retailers.model");

router.get("/mens", async (req, res) => {
  Products.find({ cat: "men" }, function (err, pdts) {
    console.log(pdts);
    res.send(pdts);
  });
});
router.get("/womens", async (req, res) => {
  Products.find({ cat: "women" }, function (err, pdts) {
    console.log(pdts);
    res.send(pdts);
  });
});

router.get("/all", async (req, res) => {
  Products.find({}, function (err, pdts) {
    if(!err){
      console.log(pdts);
      res.send(pdts);
    }
    else{
      console.log(err);
    }

  });
});


router.post("/update", async (req, res) => {
  const { id, amount } = req.body.body;
  Products.findOneAndUpdate({ id: id }, { price: amount }, function (err) {
    if (err) throw err;
  });
  res.status(200).json("Price SuccessFully Updated");
});


router.post("/updateproduct", async (req, res) => {
  const data = req.body.body;
  const filter = { _id: data._id };
  const update = { name: data.productname, price: data.price, image: data.image, cat: data.cat};
  Products.findOneAndUpdate( filter, update, {new: true}, function (err,updated) {
    if(!err){
      console.log("updated doc ",updated);
    }
  });
  res.status(200).json("Product SuccessFully Updated");
});

router.post("/addproduct", async (req, res) => {
  console.log("adding product");
  const data = req.body.body;
  console.log(data);
  const productid =  data.productid;
  const productname = data.productname;
  const price = data.price;
  const image =  data.image;
  const cat = data.cat;

  const newProduct = Products({
    id: productid,
    name: productname,
    price: price,
    image: image,
    cat: cat
  })
  newProduct.save(function(err){
    if(!err){
      console.log("adding to the database")
    }
    else{
      console.log(err);
    }
  });  
  res.status(200).json("Product SuccessFully Added");

});

router.post("/deleteproduct", async (req, res) => {
  console.log("deleting product");
  const data = req.body.body;
  const itemName = data.name;
  console.log(data);
  Products.findOneAndDelete({name: itemName }, function (err, item) {
    if (err){
        console.log(err)
    }
    else{
        console.log("Deleted Item : ",item);
        res.status(200).json("Product Deleted Successfully");
    }
});
  // Products.findOneAndDelete()


});

module.exports = router;
