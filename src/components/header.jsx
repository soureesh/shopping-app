import React from "react";
import HeaderDesk from "./headerDesk";
import HeaderMob from "./headerMob";
import "../static/bootstrap.min.css";
import "../static/style.css";
class Header extends React.Component {
  state = {
    flag: true,
  };
  handleMenu = (e) => {
    e.preventDefault();
    console.log("inside");
    if (this.state.flag) {
      console.log(this.state.flag);
      this.setState({ flag: false });
    } else {
      console.log(this.state.flag);
      this.setState({ flag: true });
    }
  };
  render() {
    return (
      <header
        id="site-header"
        className="site-header mobile-header-blue header-style-1"
      >
        <HeaderDesk {...this.props} />
        <HeaderMob {...this.props} />
      </header>
    );
  }
}

export default Header;
