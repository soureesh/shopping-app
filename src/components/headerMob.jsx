import React from "react";
import { Redirect } from "react-router-dom";

class HeaderMob extends React.Component {
  state = {
    flag: false,
    cart: false,
  };
  handleMenu = (e) => {
    e.preventDefault();
    if (this.state.flag) {
      this.setState({ flag: false });
    } else {
      this.setState({ flag: true });
    }
  };
  handleClick = (e) => {
    e.preventDefault();
    localStorage.removeItem("shop-auth-token");
    window.location = "/";
  };
  render() {
    return (
      <>
        {this.state.cart && (
          <Redirect to={{ pathname: "/cart", state: { checkout: true } }} />
        )}
        <div className="header_mobile">
          <div className="mlogo_wrapper clearfix">
            <div className="mobile_logo">
              <h3 style={{ color: "whitesmoke" }}>StarkStore</h3>
              <span className="text-light">One Stop Shopping Destination</span>
            </div>
            <div id="mmenu_toggle" onClick={this.handleMenu}>
              <button onClick={this.handleMenu}></button>
            </div>
          </div>
          <div className="mmenu_wrapper">
            <div
              className="mobile_nav"
              style={{
                display: this.state.flag ? "block" : "none",
              }}
            >
              <ul id="menu-main-menu" className="mobile_mainmenu">
                <li>
                  <a href="/">Home</a>
                </li>
                <li>
                  <a href="/mens">Men's Collection</a>
                </li>
                <li>
                  <a href="/womens">Women's Collection</a>
                </li>
              </ul>
              <div className="search-cart-box">
                {(localStorage.getItem("shop-auth-token") && (
                  <a href="/">
                    <button
                      onClick={(e) => this.handleClick(e)}
                      className="btn_landing2"
                      style={{ height: "auto" }}
                    >
                      Sign Out
                    </button>
                  </a>
                )) || (
                  <>
                    <button
                      className="btn_landing2"
                      style={{ height: "auto" }}
                      onClick={() => {
                        window.location = "/cart";
                      }}
                    >
                      Cart
                    </button>{" "}
                    <a href="/signin">
                      <button
                        className="btn_landing2"
                        style={{ height: "auto" }}
                      >
                        Login
                      </button>
                    </a>{" "}
                    <a href="/signup">
                      <button
                        className="btn_landing2"
                        style={{ height: "auto" }}
                      >
                        SignUp
                      </button>
                    </a>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default HeaderMob;
