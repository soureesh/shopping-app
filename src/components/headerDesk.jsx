import React from "react";
import { Redirect } from "react-router-dom";

class HeaderDesk extends React.Component {
  constructor(props) {
    super(props);
    console.log("header - " + props.auth);
    this.state = {
      data: {},
      doRedirect: false,
      cart: false,
      change: 0,
    };
    setInterval(() => {
      this.change === 0
        ? this.setState({ change: 1 })
        : this.setState({ change: 0 });
    });
  }
  handleClick = (e) => {
    e.preventDefault();
    localStorage.removeItem("shop-auth-token");
    localStorage.removeItem("email");
    window.location = "/";
  };
  render() {
    return (
      <>
        <div className="main-header md-hidden sm-hidden">
          <div className="main-header-top">
            <div className="container">
              <div className="d-flex align-items-center justify-content-center">
                <div id="site-logo">
                  <div id="header_name">
                    <h3 style={{ color: "whitesmoke" }} className="text-center">
                      StarkStore
                      <h6 style={{ color: "whitesmoke" }}>
                        One Stop Shopping Destination
                      </h6>
                    </h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="main-header md-hidden sm-hidden">
          <div className="main-header-bottom">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="header-mainnav">
                    <div className="search-cart-box fright">
                      {(localStorage.getItem("shop-auth-token") && (
                        <>
                          <a href="/cart">
                            <button
                              className="btn_landing2"
                              style={{ height: "auto" }}
                            >
                              Cart
                            </button>
                          </a>{" "}
                          <a href="/">
                            <button
                              onClick={(e) => this.handleClick(e)}
                              className="btn_landing2"
                              style={{ height: "auto" }}
                            >
                              Sign Out
                            </button>
                          </a>
                        </>
                      )) || (
                        <>
                          <button
                            className="btn_landing2"
                            style={{ height: "auto" }}
                            onClick={() => {
                              window.location = "/cart";
                            }}
                          >
                            Cart
                          </button>{" "}
                          <a href="/signin">
                            <button
                              className="btn_landing2"
                              style={{ height: "auto" }}
                            >
                              Login
                            </button>
                          </a>{" "}
                          <a href="/signup">
                            <button
                              className="btn_landing2"
                              style={{ height: "auto" }}
                            >
                              SignUp
                            </button>
                          </a>
                        </>
                      )}
                      {localStorage.getItem("email") &&
                      localStorage
                        .getItem("email")
                        .includes("admin@gmail.com") ? (
                        <>
                          <a href="/addProduct">
                            <button
                              className="btn_landing2"
                              style={{ height: "auto", marginLeft: 5 }}
                            >
                              Add
                            </button>
                          </a>{" "}
                          <a href="/deleteProduct">
                            <button
                              className="btn_landing2"
                              style={{ height: "auto" }}
                            >
                              Manage
                            </button>
                          </a>{" "}
                        </>
                      ) : (
                        <></>
                      )}
                    </div>

                    <div id="site-navigation" className="main-navigation fleft">
                      <ul id="primary-menu" className="menu">
                        <li>
                          <a href="/">Home</a>
                        </li>
                        <li>
                          <a href="/mens">Men's Collection</a>
                        </li>
                        <li>
                          <a href="/womens">Women's Collection</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default HeaderDesk;
