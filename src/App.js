import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Home from "./pages/Home";
import Header from "./components/header";
import SignIn from "./pages/SignIn";
import SignUp from "./pages/SignUp";
import Mens from "./pages/Mens";
import Womens from "./pages/Womens";
import Product from "./pages/Product";
import Cart from "./pages/Cart";
import Buy from "./pages/Buy";
import Update from "./pages/Update"
import Master from "./pages/master";
import AdminSignIn from "./pages/adminSignIn";
import AddProduct from "./pages/addProduct";
import DeleteProduct from "./pages/deleteProduct";
import AccessDenied from "./pages/accessDenied";

import "./static/bootstrap.min.css";

function App() {
  return (
    <>
      <Header />
      <Router>
        <Switch>
          <Route path="/signin">
            <SignIn />
          </Route>
          <Route path="/signup">
            <SignUp />
          </Route>
          <Route path="/mens">
            <Mens />
          </Route>
          <Route path="/womens">
            <Womens />
          </Route>
          <Route path="/product">
            <Product />
          </Route>
          <Route path="/cart">
            <Cart />
          </Route>
          <Route path="/buy">
            <Buy />
          </Route>
          <Route path="/update">
            <Update />
          </Route>
          <Route path="/master">
            <Master />
          </Route>
          <Route path="/admin">
            <AdminSignIn />
          </Route>
          <Route path="/addproduct">
            <AddProduct />
          </Route>
          <Route path="/deleteProduct">
            <DeleteProduct />
          </Route>
          <Route path="/accessDenied">
            <AccessDenied />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </>
  );
}

export default App;
