import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
} from "reactstrap";
import PasswordField from "material-ui-password-field";
export default function SignUp() {
  const handleSubmit = () => {
    if (password !== conf) {
      alert("Passwords Don't Match !");
      return;
    }
    axios
      .post("http://localhost:5000/api/users/register", {
        body: {
          lastName: lastName,
          firstName: firstName,
          sex: sex,
          birth_date: birth_date,
          age: age,
          email: email,
          password: password,
          mob: mob,
        },
      })
      .then((response) => {
        console.log(response);
        alert("You Are Successfully Registered");
        window.location.href = "/signin";
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const [lastName, set_lastName] = useState("");
  const [firstName, set_firstName] = useState("");
  const [sex, set_sex] = useState("");
  const [birth_date, set_birth_date] = useState("");
  const [age, set_age] = useState(0);
  const [email, set_email] = useState("");
  const [mob, set_mob] = useState("");
  const [password, setPassword] = useState("");
  const [conf, setconfPassword] = useState("");

  window.scrollTo(0, 0);

  return (
    <Form id="myForm">
      <Row>
        <Col>
          <Card>
            <CardHeader>
              <h3>
                <center>Personal Details</center>
              </h3>
            </CardHeader>
            <CardBody>
              <FormGroup>
                <Row>
                  <Col xs={12} sm={12} md={6}>
                    <Label for="exampleFirst"> First Name</Label>
                    <Input
                      type="text"
                      onChange={(e) => set_firstName(e.target.value)}
                      value={firstName}
                      name="firstname"
                      required
                    />
                  </Col>
                  <Col xs={12} sm={12} md={6}>
                    <Label for="exampleLast">Last Name</Label>
                    <Input
                      type="text"
                      onChange={(e) => set_lastName(e.target.value)}
                      value={lastName}
                      name="surname"
                      required
                    />
                  </Col>
                </Row>
              </FormGroup>

              <FormGroup>
                <Row>
                  <Col xs={12} md={6}>
                    <Label for="exampleDate">Date of Birth</Label>
                    <Input
                      type="date"
                      value={birth_date}
                      name="birth_date"
                      required
                      onChange={(e) => {
                        let today = new Date();
                        let [year, month, day] = e.target.value.split("-");
                        year = Number(year);
                        month = Number(month);
                        day = Number(day);

                        let tempAge = today.getFullYear() - year;
                        if (
                          today.getMonth() < month ||
                          (today.getMonth() === month && today.getDate() < day)
                        ) {
                          tempAge--;
                        }
                        set_age(tempAge);
                        set_birth_date(e.target.value);
                      }}
                      id="exampledate"
                      placeholder="date placeholder"
                    />
                  </Col>
                  <Col xs={6} md={3}>
                    <Label for="exampleNumber">Age</Label>
                    <Input
                      type="number"
                      value={age}
                      required
                      name="age"
                      onChange={(e) => set_age(e.target.value)}
                      id="examplenumber"
                      disabled
                    />
                  </Col>
                  <Col xs={6} md={3}>
                    <Label for="exampleSelect">Sex</Label>
                    <Input
                      type="select"
                      onChange={(e) => set_sex(e.target.value)}
                      value={sex}
                      name="sex"
                      required
                    >
                      <option value="">-----</option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                    </Input>
                  </Col>
                </Row>
              </FormGroup>

              <FormGroup>
                <Row>
                  <Col xs={12} md={6}>
                    <Label for="exampleEmail">Email</Label>
                    <Input
                      type="email"
                      value={email}
                      required
                      name="email"
                      onChange={(e) => set_email(e.target.value)}
                      // placeholder=""
                    />
                  </Col>
                  <Col xs={12} md={6}>
                    <Label for="examplephone">Mobile no.</Label>
                    <Input
                      type="text"
                      value={mob}
                      required
                      name="mob"
                      onChange={(e) => set_mob(e.target.value)}
                      id="examplephone"
                    />
                  </Col>
                </Row>
              </FormGroup>
              <FormGroup>
                <Row>
                  <Col xs={12} md={6}>
                    <Label for="exampleEmail">Password</Label>
                    <Input
                      type="password"
                      value={password}
                      required
                      name="password"
                      onChange={(e) => setPassword(e.target.value)}
                    />
                  </Col>

                  <Col xs={12} md={6}>
                    <Label for="exampleEmail">Confirm Password</Label>
                    <Input
                      type="password"
                      required
                      value={conf}
                      name="password"
                      onChange={(e) => setconfPassword(e.target.value)}
                    />
                  </Col>
                </Row>
              </FormGroup>
              <FormGroup>
                <Row className="d-flex justify-content-center">
                  <Button
                    onClick={(e) => {
                      e.preventDefault();
                      handleSubmit();
                    }}
                    fullWidth
                    variant="contained"
                    color="primary"
                    type="submit"
                  >
                    Sign Up
                  </Button>
                </Row>
              </FormGroup>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Form>
  );
}
