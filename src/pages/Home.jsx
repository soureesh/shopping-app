import React, { useState, useEffect } from "react";
import { Row, Col } from "reactstrap";
import logo from "./../logo.svg";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";

import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";

import Typography from "@material-ui/core/Typography";
import mens from "./../assets/mens.jpg";
import womens from "./../assets/womens.jpg";
import { Grid, TextField } from "@material-ui/core";
import { Redirect } from "react-router-dom";
import PasswordField from "material-ui-password-field";
import { withRouter } from "react-router-dom";
import { Input } from "reactstrap";
// import Avatar from "material-ui/core/Avatar";
// import LockOutlinedIcon from "material-ui/icons/LockOutlined";
import useStyles from "../static/signin";

export default function Home() {
  var [route, setRoute] = useState("");

  const classes = useStyles();

  return (
    <Grid
      container
      className={classes.container}
      style={{ width: "auto", margin: "3%", backgroundColor: "transparent" }}
    >
      {route === "mens" && <Redirect to="/mens" />}
      {route === "womens" && <Redirect to="/womens" />}
      <div className={classes.formContainer}>
        <div className={classes.form}>
          <React.Fragment>
            <div className={classes.paper}>
              <Row
                className={classes.container}
                style={{ width: "fit-content" }}
              >
                <Col xs={12} sm={6}>
                  <div onClick={() => setRoute("mens")}>
                    <Card style={{ maxWidth: 345 }}>
                      <CardActionArea>
                        <CardMedia
                          component="img"
                          alt="Menware"
                          height="140"
                          image={mens}
                          title="Men Ware"
                        />
                        <CardContent>
                          <Typography gutterBottom variant="h5" component="h2">
                            Discover Menware
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </div>
                </Col>

                <Col xs={12} sm={6}>
                  <div onClick={() => setRoute("womens")}>
                    <Card style={{ maxWidth: 345 }}>
                      <CardActionArea>
                        <CardMedia
                          component="img"
                          alt="Menware"
                          height="140"
                          image={womens}
                          title="Women Ware"
                        />
                        <CardContent>
                          <Typography gutterBottom variant="h5" component="h2">
                            Discover Women Ware
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </div>
                </Col>
              </Row>
            </div>
          </React.Fragment>
        </div>
      </div>
    </Grid>
  );
}
