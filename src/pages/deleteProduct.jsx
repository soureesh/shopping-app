import React, { useState, useEffect } from "react";
import { Row, Col } from "reactstrap";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import { Redirect } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { Grid, TextField } from "@material-ui/core";
import axios from "axios";
import useStyles from "../static/signin";
export default function Womens() {
  var [prods, setProds] = useState([]);
  const [route, setRoute] = useState(null);
  const [buy, setBuy] = useState(null);
  const [update, setUpdate] = useState(null);
  const [redirectDash, doRedirectDash] = useState(false);

  const classes = useStyles();

  if (localStorage.getItem("email") != '"admin@gmail.com"') {
    window.location.href = "/accessDenied";
  }

  const deleteItem = (item) => {
    axios
      .post("http://localhost:5000/api/commons/deleteproduct", {
        body: {
          name: item.name,
          price: item.price,
        },
      })
      .then((response) => {
        console.log(response);
        doRedirectDash(true);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const updateItem = (item) => {
    // axios
    // .post("http://localhost:5000/api/commons/updateproduct", {
    //   body: {
    //       name: item.name,
    //       price: item.price
    //   },
    // })
    // .then((response) => {
    //   console.log(response);
    //   doRedirectDash(true);
    // })
    // .catch((err) => {
    //   console.log(err);
    // });
    const ls = require("local-storage");
    ls("current", JSON.stringify(item));
    window.location = "/update";
  };

  useEffect(() => {
    axios
      .get("http://localhost:5000/api/commons/all")
      .then((res) => {
        console.log(res.data);
        setProds(res.data);
      })
      .catch((err) => console.log(err));
  }, [buy]);
  var items = prods.map((data) => {
    const { _id, name, price, image } = data;
    return (
      <Col xs={12} sm={3}>
        <Card style={{ margin: "1.5%", height: "fit-content" }}>
          <CardActionArea style={{ maxWidth: 345 }}>
            <div onClick={() => setRoute(data)}>
              <CardMedia
                component="img"
                alt="image"
                height="140"
                image={image}
                title="Clothing"
              />
              <CardContent>
                <Typography gutterBottom variant="h6" component="h6">
                  {name}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  INR{price}
                </Typography>
              </CardContent>
            </div>
            <CardActions>
              <Button
                size="small"
                color="primary"
                onClick={(e) => {
                  e.preventDefault();
                  deleteItem(data);
                  alert("Item will be deleted");
                }}
              >
                Delete
              </Button>
              <Button
                size="small"
                color="primary"
                onClick={(e) => {
                  e.preventDefault();
                  updateItem(data);
                  // alert("Item will be deleted");
                }}
              >
                Update
              </Button>
            </CardActions>
          </CardActionArea>
        </Card>
      </Col>
    );
  });
  return (
    <>
      {update !== null && (
        <Redirect to={{ pathname: "/update", state: update }} />
      )}
      {redirectDash === true && <Redirect to="/" />}
      <center style={{ margin: "5%" }}>
        <Typography gutterBottom variant="h5" component="h6">
          Manage Items in Store
        </Typography>
      </center>
      <Grid
        container
        className={classes.container}
        style={{ width: "auto", margin: "3%", backgroundColor: "transparent" }}
      >
        <div className={classes.formContainer}>
          <div className={classes.form}>
            <React.Fragment>
              <div className={classes.paper}>
                <Row className={classes.container}>{items}</Row>
              </div>
            </React.Fragment>
          </div>
        </div>
      </Grid>
    </>
  );
}
