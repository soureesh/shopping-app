import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { Row, Col, CardBody } from "reactstrap";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import { Redirect } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { Grid, TextField } from "@material-ui/core";
import axios from "axios";

export default function Buy() {
  const location = useLocation();
  const { name, image, price } = location.state;
  return (
    <>
      <Row xs={12} style={{ margin: "5%" }}>
        <Card xs={12} style={{ width: "100%", marginBottom: "2%" }}>
          <Row
            xs={12}
            style={{
              width: "100%",
            }}
          >
            <Col
              xs={12}
              md={3}
              className="d-flex justify-content-center align-items-center"
            >
              <img
                style={{
                  height: 130,
                  width: 130,
                  margin: "1%",
                }}
                name="img"
                alt={name}
                src={image}
              />
            </Col>
            <Col
              xs={12}
              md={4}
              style={{ margin: "1%" }}
              className="col align-items-center justify-content-center"
            >
              <Typography variant="h5">{name}</Typography>
              <Typography variant="h5" color="textSecondary" component="h4">
                Price - INR {price}
              </Typography>
            </Col>
            <Col xs={12} md={3} style={{ marginTop: "2%" }}>
              <Button
                size="large"
                className="float-right"
                variant="contained"
                color="primary"
                onClick={(e) => {
                  e.preventDefault();
                  localStorage.removeItem("cart");
                  alert("Your Order Is Placed");
                  window.location = "/";
                }}
              >
                Check Out
              </Button>
            </Col>
          </Row>
        </Card>
      </Row>
    </>
  );
}
