import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { Row, Col, CardBody } from "reactstrap";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import { Redirect } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { Grid, TextField } from "@material-ui/core";
import axios from "axios";

import useStyles from "../static/signin";
export default function Cart() {
  var quant = {};

  const location = useLocation();
  const [route, setRoute] = useState(null);
  const classes = useStyles();
  var [items, setItems] = useState(null);
  var [quant, setQuant] = useState({});
  var [amt, setAmt] = useState(0);
  var [no, setNo] = useState(0);
  var [update, trigger] = useState(1);
  var [empty, setEmpty] = useState(false);
  const addItem = (id) => {
    var state = quant;
    state[id]["no"] += 1;

    console.log(state);
    setQuant(state);
    setValues();
    var arr = [];

    JSON.parse(localStorage.getItem("cart"))["data"].forEach((item) => {
      if (item.id === id) {
        console.log(item);
        item.no = state[id]["no"];
      }
      arr.push(item);
    });
    console.log(arr);
    localStorage.setItem("cart", JSON.stringify({ data: arr }));
    update === 0 ? trigger(1) : trigger(0);
  };
  const diffItem = (id) => {
    var state = quant;
    state[id]["no"] -= 1;
    state[id]["no"] = state[id]["no"] < 0 ? 0 : state[id]["no"];
    console.log(state);
    setQuant(state);
    setValues();
    var arr = [];
    JSON.parse(localStorage.getItem("cart"))["data"].forEach((item) => {
      if (item.id === id) {
        console.log(item);
        item.no = state[id]["no"];
      }
      if (item.no !== 0) arr.push(item);
    });
    console.log(arr);
    localStorage.setItem("cart", JSON.stringify({ data: arr }));
    update === 0 ? trigger(1) : trigger(0);
  };
  const setValues = () => {
    var amt = 0;
    var no = 0;
    Object.keys(quant).forEach((key) => {
      no += parseInt(quant[key].no);
      no = no < 0 ? 0 : no;
      amt += parseInt(quant[key].price) * parseInt(quant[key].no);
    });

    setAmt(amt);
    setNo(no);
  };
  useEffect(() => {
    if (!localStorage.getItem("cart")) {
      setEmpty(true);
      return;
    }
    var data = JSON.parse(localStorage.getItem("cart"))["data"];

    var items = data.map((item) => {
      const { id, name, no, price, image } = item;

      quant[id] = { price: price, no: no };

      return (
        <>
          <Card xs={12} style={{ width: "100%", marginBottom: "2%" }}>
            <Row
              xs={12}
              style={{
                width: "100%",
              }}
            >
              <Col
                xs={12}
                md={3}
                className="d-flex justify-content-center align-items-center"
              >
                <img
                  style={{
                    height: 130,
                    width: 130,
                    margin: "1%",
                  }}
                  name="img"
                  alt={name}
                  src={image}
                />
              </Col>
              <Col xs={12} md={4} style={{ margin: "1%" }}>
                <Typography variant="h5">{name}</Typography>
                <Typography variant="h5" color="textSecondary" component="h4">
                  Price - INR {price}
                </Typography>
              </Col>
              <Col xs={12} md={2} style={{ margin: "1%" }}>
                <Typography variant="h6">Quantity - {no}</Typography>
              </Col>
              <Col xs={12} md={2} style={{ margin: "1%" }}>
                <Button
                  variant="outlined"
                  style={{ marginRight: "1" }}
                  onClick={(e) => {
                    e.preventDefault();
                    addItem(id);
                  }}
                >
                  +
                </Button>
                <Button
                  variant="outlined"
                  onClick={(e) => {
                    e.preventDefault();
                    diffItem(id);
                  }}
                >
                  -
                </Button>
              </Col>
            </Row>
          </Card>
        </>
      );
    });
    setItems(items);
    setQuant(quant);
    setValues();
  }, [update]);
  return (
    <>
      {empty ? (
        <>
          <center style={{ margin: "5%" }}>
            <Typography variant="h3">Oops !! Its all Empty Here</Typography>
          </center>
        </>
      ) : (
        <div>
          <Row xs={12} style={{ margin: "5%" }}>
            {items}
          </Row>
          <Row xs={12} style={{ margin: "5%", flexDirection: "row" }}>
            <Col xs={6} md={3} style={{ marginTop: "2%" }}>
              <Typography variant="h6">Total Quantity - {no}</Typography>
            </Col>
            <Col xs={6} md={3} style={{ marginTop: "2%" }}>
              <Typography variant="h6">Total Amount - INR {amt}</Typography>
            </Col>
            <Col xs={12} md={6} style={{ marginTop: "2%" }}>
              <Button
                size="large"
                className="float-right"
                variant="contained"
                color="primary"
                onClick={(e) => {
                  e.preventDefault();
                  if (!localStorage.getItem("shop-auth-token")) {
                    alert("Please log in to place order !");

                    return;
                  }
                  localStorage.removeItem("cart");
                  alert("Your Order Is Placed");
                  window.location = "/";
                }}
              >
                Check Out
              </Button>
            </Col>
          </Row>
        </div>
      )}
    </>
  );
}
