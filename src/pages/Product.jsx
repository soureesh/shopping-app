import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { Row, Col } from "reactstrap";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import { Redirect } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { Grid, TextField } from "@material-ui/core";
import axios from "axios";

import useStyles from "../static/signin";
export default function Product() {
  const location = useLocation();
  const [route, setRoute] = useState(null);
  const classes = useStyles();
  const [buy, setBuy] = useState(null);
  const addItem = (item) => {
    var cart = {};
    if (localStorage.getItem("cart")) {
      var data = JSON.parse(localStorage.getItem("cart"))["data"];
      var arr = [];
      var flag = 0;
      data.forEach((pdt) => {
        if (pdt.id === item.id) {
          pdt["no"] += 1;
          flag = 1;
        }
        arr.push(pdt);
      });
      if (flag === 0) {
        item["no"] = 1;
        arr.push(item);
      }
      cart["data"] = arr;
      localStorage.setItem("cart", JSON.stringify(cart));
      console.log(data);
    } else {
      item["no"] = 1;
      cart["data"] = [item];
      localStorage.setItem("cart", JSON.stringify(cart));
    }
  };
  return (
    <>
      {buy !== null && <Redirect to={{ pathname: "/buy", state: buy }} />}
      <center style={{ margin: "5%" }}>
        {/* <Typography gutterBottom variant="h5" component="h6">
          Mens' Authentic Collection
        </Typography> */}
      </center>
      <Grid
        container
        className={classes.container}
        style={{ width: "auto", margin: "3%", backgroundColor: "transparent" }}
      >
        <div className={classes.formContainer}>
          <div className={classes.form}>
            <React.Fragment>
              <div className={classes.paper}>
                <Row style={{ flexDirection: "row" }}>
                  <Col sm={12} md={5}>
                    <Card style={{ minWidth: 360 }}>
                      <CardActionArea>
                        <CardMedia
                          component="img"
                          alt={location.state.name}
                          height="280"
                          image={location.state.image}
                        />
                      </CardActionArea>
                    </Card>
                  </Col>
                  <Col sm={12} md={7}>
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h5">
                        {location.state.name}
                      </Typography>
                      <Typography
                        variant="h5"
                        color="textSecondary"
                        component="h4"
                      >
                        INR {location.state.price}
                      </Typography>
                    </CardContent>
                    <CardActions>
                      <Button
                        variant="contained"
                        size="large"
                        color="primary"
                        onClick={(e) => {
                          e.preventDefault();
                          setBuy(location.state);
                        }}
                      >
                        Buy Now
                      </Button>
                      <Button
                        variant="contained"
                        size="large"
                        color="secondary"
                        onClick={(e) => {
                          e.preventDefault();
                          addItem(location.state);
                          alert("Item Added to Cart");
                        }}
                      >
                        Add to Cart
                      </Button>
                    </CardActions>
                  </Col>
                </Row>
              </div>
            </React.Fragment>
          </div>
        </div>
      </Grid>
    </>
  );
}
