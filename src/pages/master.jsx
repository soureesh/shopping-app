import React, { useState, useEffect } from "react";
import { Row, Col } from "reactstrap";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import { Redirect } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { Grid, TextField, Input } from "@material-ui/core";
import axios from "axios";

import useStyles from "../static/signin";

export default function Master() {
  var [prods, setProds] = useState([]);
  var [price, setPrice] = useState({});
  var [change, setChange] = useState(1);
  var old = {};
  useEffect(() => {
    var temp = [];
    var tmp = {};
    axios
      .get("http://localhost:5000/api/commons/mens")
      .then((res) => {
        console.log(res.data);
        res.data.forEach((item) => {
          temp.push(item);
          tmp[item.id] = item.price;
        });
        axios
          .get("http://localhost:5000/api/commons/womens")
          .then((res) => {
            console.log(res.data);
            res.data.forEach((item) => {
              temp.push(item);
              tmp[item.id] = item.price;
            });
            setPrice(tmp);
            setProds(temp);
          })
          .catch((err) => console.log(err));
      })
      .catch((err) => console.log(err));
  }, [change]);
  const update = (id) => {
    axios
      .post("http://localhost:5000/api/commons/update", {
        body: { id: id, amount: price[id] },
      })
      .then((res) => {
        alert("Price Updated");
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <>
      <center style={{ margin: "5%" }}>
        <Typography gutterBottom variant="h5" component="h6">
          Modify Inventory Here
        </Typography>
      </center>
      <Row xs={12} style={{ margin: "5%" }}>
        {prods.map((data) => {
          const { id, name, image } = data;
          return (
            <Card
              xs={12}
              style={{ width: "100%", marginBottom: "2%" }}
              key={id}
            >
              <Row
                xs={12}
                style={{
                  width: "100%",
                }}
              >
                <Col
                  xs={12}
                  md={3}
                  className="d-flex justify-content-center align-items-center"
                >
                  <img
                    style={{
                      height: 130,
                      width: 130,
                      margin: "1%",
                    }}
                    name="img"
                    alt={name}
                    src={image}
                  />
                </Col>
                <Col
                  xs={12}
                  md={4}
                  style={{ margin: "1%" }}
                  className="col align-items-center justify-content-center"
                >
                  <Typography variant="h5">{name}</Typography>
                  <Input
                    value={price[id]}
                    type="text"
                    name={id}
                    id={id}
                    onChange={(e) => {
                      old[id] = e.target.value;
                      setPrice(old);
                    }}
                  />
                </Col>
                <Col xs={12} md={3} style={{ marginTop: "2%" }}>
                  <Button
                    size="large"
                    className="float-right"
                    variant="contained"
                    color="primary"
                    onClick={(e) => {
                      e.preventDefault();
                      update(id);
                      change === 0 ? setChange(1) : setChange(0);
                    }}
                  >
                    Update
                  </Button>
                </Col>
              </Row>
            </Card>
          );
        })}
      </Row>
    </>
  );
}
