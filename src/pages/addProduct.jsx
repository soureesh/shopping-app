import React from "react";

import { Grid, Typography, Button } from "@material-ui/core";
import { Redirect } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { Input } from "reactstrap";
import useStyles from "../static/signin";

import { useState } from "react";
const axios = require("axios");

function AddProduct() {
  var ls = require("local-storage");
  if( localStorage.getItem("email") != "\"admin@gmail.com\""){
    window.location.href = "/accessDenied"
  }

  var classes = useStyles();

  const [productid, setProductId] = useState("");
  const [productname, setProductName] = useState("");
  const [price, setPrice] = useState("");
  const [image, setImage] = useState("");
  const [cat, setCat] = useState("");
  const [redirectDash, doRedirectDash] = useState(false);

  const handleChange = (name) => (event) => {
    if (name === "productid") setProductId(event.target.value);
    else if (name === "productname") setProductName(event.target.value);
    else if (name === "price") setPrice(event.target.value);
    else if (name === "image") setImage(event.target.value);
    else if (name === "cat") setCat(event.target.value);
  };

  const add = () => {
    axios
      .post("http://localhost:5000/api/commons/addproduct", {
        body: {
          productid: productid,
          productname: productname,
          price: price,
          image: image,
          cat: cat,
        },
      })
      .then((response) => {
        console.log(response);
        // ls("shop-auth-token", response.data.token);
        // console.log(ls.get("shop-auth-token"));
        doRedirectDash(true);
      })
      .catch((err) => {
        // console.log(Object.keys(err));
        // alert("Invalid Credentials");
        console.log(err);
      });
  };

  return (
    <Grid
      container
      className={classes.container}
      style={{ width: "auto", margin: "3%", backgroundColor: "transparent" }}
    >
      {redirectDash === true && <Redirect to="/" />}
      <div className={classes.formContainer}>
        <div className={classes.form}>
          <React.Fragment>
            <div className={classes.paper}>
              {/* <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar> */}
              <Typography component="h1" variant="h5">
                Add Product
              </Typography>
              <form className={classes.form2} noValidate>
                <Grid
                  container
                  alignItems="center"
                  justify="center"
                  spacing={2}
                >
                  <Grid
                    item
                    xs={12}
                    container
                    alignItems="center"
                    justify="center"
                    spacing={2}
                  >
                    <Grid item xs={12} sm={8}>
                      <h6>Product ID</h6>
                      <Input
                        autoComplete="productid"
                        name="productid"
                        variant="outlined"
                        value = {productid}
                        required
                        onChange={handleChange("productid")}
                        fullWidth
                        id="productid"
                        autoFocus
                        // variant="outlined"
                      />
                      {/* <FormHelperText>User ID</FormHelperText> */}
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    container
                    alignItems="center"
                    justify="center"
                    spacing={2}
                  >
                    <Grid item xs={12} sm={8}>
                      <h6>Name</h6>
                      <Input
                        autoComplete="productname"
                        name="productname"
                        value= {productname}
                        variant="outlined"
                        required
                        onChange={handleChange("productname")}
                        fullWidth
                        id="productname"
                      />
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    container
                    alignItems="center"
                    justify="center"
                    spacing={2}
                  >
                    <Grid item xs={12} sm={8}>
                      <h6>Price</h6>
                      <Input
                        autoComplete="price"
                        name="price"
                        variant="outlined"
                        value= {price}
                        required
                        onChange={handleChange("price")}
                        fullWidth
                        id="price"
                      />
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    container
                    alignItems="center"
                    justify="center"
                    spacing={2}
                  >
                    <Grid item xs={12} sm={8}>
                      <h6>Image URL</h6>
                      <Input
                        autoComplete="image"
                        name="image"
                        variant="outlined"
                        value= { image }
                        required
                        onChange={handleChange("image")}
                        fullWidth
                        id="image"
                      />
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    container
                    alignItems="center"
                    justify="center"
                    spacing={2}
                  >
                    <Grid item xs={12} sm={8}>
                      <h6>Category</h6>
                      <Input
                        type= "select"
                        value = { cat }
                        autoComplete="cat"
                        name="cat"
                        variant="outlined"
                        required
                        onChange={handleChange("cat")}
                        fullWidth
                        id="cat"
                      >
                        <option value="">-----</option>
                        <option value="men">Men</option>
                        <option value="women">Women</option>

                      </Input>
                      
                    </Grid>
                  </Grid>
                  
                  <Grid
                    container
                    spacing={2}
                    alignItems="center"
                    justify="center"
                    sm={4}
                    xs={6}
                  >
                    <Button
                      onClick={(e) => {
                        e.preventDefault();
                        add();
                      }}
                      fullWidth
                      variant="contained"
                      color="primary"
                      type="submit"
                      className={classes.submit}
                    >
                      Add Product
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </div>
          </React.Fragment>
        </div>
      </div>
    </Grid>
  );
}

export default withRouter(AddProduct);
