import React from 'react';
import { Link } from 'react-router-dom';
import error from "./../assets/error.png";

class AccessDenied extends React.Component{
    render(){
        return <div>
            <center>
            
              <Link to="/"> 
              <img src={error}  />
              </Link>
            </center>
            
          </div>;
    }
}
export default AccessDenied;