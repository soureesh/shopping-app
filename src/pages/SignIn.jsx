import React from "react";

import { Grid, Typography, Button } from "@material-ui/core";
import { Redirect } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { Input } from "reactstrap";
// import Avatar from "material-ui/core/Avatar";
// import LockOutlinedIcon from "material-ui/icons/LockOutlined";
import useStyles from "../static/signin";

import { useState } from "react";
const axios = require("axios");

function SignIn() {
  var ls = require("local-storage");

  var classes = useStyles();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [redirect, doRedirect] = useState(false);

  const handleChange = (name) => (event) => {
    if (name === "username") setUsername(event.target.value);
    else if (name === "password") setPassword(event.target.value);
  };

  const authenticate = () => {
    axios
      .post("http://localhost:5000/api/users/auth", {
        body: {
          username,
          pass: password,
        },
      })
      .then((response) => {
        console.log(response);
        ls("shop-auth-token", response.data.token);
        ls("email", username);
        console.log("Username:- ", username);
        console.log("token", ls.get("shop-auth-token"));
        window.location = "/";
        // doRedirect(true);
      })
      .catch((err) => {
        console.log(Object.keys(err));
        alert("Invalid Credentials");
      });
  };

  return (
    <Grid
      container
      className={classes.container}
      style={{ width: "auto", margin: "3%", backgroundColor: "transparent" }}
    >
      {redirect === true && <Redirect to="/signin" />}
      <div className={classes.formContainer}>
        <div className={classes.form}>
          <React.Fragment>
            <div className={classes.paper}>
              {/* <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar> */}
              <Typography component="h1" variant="h5">
                Sign In
              </Typography>
              <form className={classes.form2} noValidate>
                <Grid
                  container
                  alignItems="center"
                  justify="center"
                  spacing={2}
                >
                  <Grid
                    item
                    xs={12}
                    container
                    alignItems="center"
                    justify="center"
                    spacing={2}
                  >
                    <Grid item xs={12} sm={8}>
                      <h6>User ID</h6>
                      <Input
                        autoComplete="userid"
                        name="userid"
                        variant="outlined"
                        required
                        onChange={handleChange("username")}
                        fullWidth
                        id="uid"
                        autoFocus
                        // variant="outlined"
                      />
                      {/* <FormHelperText>User ID</FormHelperText> */}
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    container
                    alignItems="center"
                    justify="center"
                    spacing={2}
                  >
                    <Grid item xs={12} sm={8}>
                      <h6>Password</h6>
                      <Input
                        autoComplete="password"
                        name="password"
                        required
                        type="password"
                        fullWidth
                        onChange={handleChange("password")}
                        id="password"
                      />
                      {/* <FormHelperText>Password</FormHelperText> */}
                    </Grid>
                  </Grid>

                  <Grid
                    container
                    spacing={2}
                    alignItems="center"
                    justify="center"
                    sm={4}
                    xs={6}
                  >
                    <Button
                      onClick={(e) => {
                        e.preventDefault();
                        authenticate();
                      }}
                      fullWidth
                      variant="contained"
                      color="primary"
                      type="submit"
                      className={classes.submit}
                    >
                      Sign In
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </div>
          </React.Fragment>
        </div>
      </div>
    </Grid>
  );
}

export default withRouter(SignIn);
