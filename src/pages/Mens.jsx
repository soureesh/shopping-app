import React, { useState, useEffect } from "react";
import { Row, Col } from "reactstrap";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import { Redirect } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { Grid, TextField } from "@material-ui/core";
import axios from "axios";

import useStyles from "../static/signin";
export default function Mens() {
  var [prods, setProds] = useState([]);
  const [route, setRoute] = useState(null);
  const classes = useStyles();
  const [buy, setBuy] = useState(null);

  const addItem = (item) => {
    var cart = {};
    if (localStorage.getItem("cart")) {
      var data = JSON.parse(localStorage.getItem("cart"))["data"];
      var arr = [];
      var flag = 0;
      data.forEach((pdt) => {
        if (pdt.id === item.id) {
          pdt["no"] += 1;
          flag = 1;
        }
        arr.push(pdt);
      });
      if (flag === 0) {
        item["no"] = 1;
        arr.push(item);
      }
      cart["data"] = arr;
      localStorage.setItem("cart", JSON.stringify(cart));
      console.log(data);
    } else {
      item["no"] = 1;
      cart["data"] = [item];
      localStorage.setItem("cart", JSON.stringify(cart));
    }
  };
  useEffect(() => {
    axios
      .get("http://localhost:5000/api/commons/mens")
      .then((res) => {
        console.log(res.data);
        setProds(res.data);
      })
      .catch((err) => console.log(err));
  }, [buy]);
  var items = prods.map((data) => {
    const { id, name, price, image } = data;
    return (
      <Col xs={12} sm={3} style={{ margin: "1.5%" }}>
        <Card style={{ maxWidth: 345 }}>
          <CardActionArea>
            <div onClick={() => setRoute(data)}>
              <CardMedia
                component="img"
                alt="Menware"
                height="140"
                image={image}
                title="Men Ware"
              />
              <CardContent>
                <Typography gutterBottom variant="h6" component="h6">
                  {name}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  INR{price}
                </Typography>
              </CardContent>
            </div>
            <CardActions>
              <Button
                size="small"
                color="primary"
                onClick={(e) => {
                  e.preventDefault();
                  setBuy(data);
                }}
              >
                Buy Now
              </Button>
              <Button
                size="small"
                color="primary"
                onClick={(e) => {
                  e.preventDefault();
                  addItem(data);
                  alert("Item Added to Cart");
                }}
              >
                Add to Card
              </Button>
            </CardActions>
          </CardActionArea>
        </Card>
      </Col>
    );
  });
  return (
    <>
      {buy !== null && <Redirect to={{ pathname: "/buy", state: buy }} />}
      {route !== null && (
        <Redirect to={{ pathname: "/product", state: route }} />
      )}
      <center style={{ margin: "5%" }}>
        <Typography gutterBottom variant="h5" component="h6">
          Mens' Authentic Collection
        </Typography>
      </center>
      <Grid
        container
        className={classes.container}
        style={{ width: "auto", margin: "3%", backgroundColor: "transparent" }}
      >
        <div className={classes.formContainer}>
          <div className={classes.form}>
            <React.Fragment>
              <div className={classes.paper}>
                <Row className={classes.container}>{items}</Row>
              </div>
            </React.Fragment>
          </div>
        </div>
      </Grid>
    </>
  );
}
